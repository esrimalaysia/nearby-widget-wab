/**
 * Created by mkmoey on 5/5/2016.
 */
define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./Nearby.html',
    './js/FindCentrePoint',
    './js/POIGrid',
    './js/POIList',
    "esri/layers/GraphicsLayer",
    'dojo/on',
    'dojo/_base/lang',
    "dojo/aspect",
], function (declare,
             _WidgetBase,
             _TemplatedMixin,
             _WidgetsInTemplateMixin,
             nearbyTemplate,
             CentrePoint,
             POIGrid,
             POIList,
             GraphicsLayer,
             on,
             lang) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        map: null,
        baseClass: "nearbyDijit",
        widgetsInTemplate: true,
        templateString: nearbyTemplate,
        appConfig: this.appConfig,
        config: this.config,
        bufferGeometry: null,
        containerDimensions: null,
        featureSelectLayer: null,

        postCreate: function () {
            this.inherited(arguments);

            //Find Centre Point Dijit setup
            var xBufferLayer = new esri.layers.GraphicsLayer();
            this.map.addLayer(xBufferLayer);

            //Layer for all the selection POI graphics
            this.featureSelectLayer = new esri.layers.GraphicsLayer();
            this.map.addLayer(this.featureSelectLayer);

            this.centrePoint = new CentrePoint({
                map: this.map,
                config: this.config,
                nls: this.nls,
                xBufferLayer: xBufferLayer
            });
            this.centrePoint.own(on(this.centrePoint, "centreSelected", lang.hitch(this, this.onCentreSelected)));
            this.centrePoint.own(on(this.centrePoint, "expanderToggled", lang.hitch(this, this.onAnyExpanderToggled)));

            this.centrePoint.placeAt(this.findCentrePointNode);
            this.centrePoint.startup();

            //POI Grid Dijit setup
            this.poiGrid = new POIGrid({
                map: this.map,
                config: this.config,
                nls: this.nls,
                featureSelectGraphicLayer: this.featureSelectLayer
            });

            this.poiGrid.placeAt(this.poiGridNode);
            this.poiGrid.startup();

            this.poiGrid.own(on(this.poiGrid, "POICellChecked", lang.hitch(this, this.onPOIChecked)));
            this.poiGrid.own(on(this.poiGrid, "expanderToggled", lang.hitch(this, this.onAnyExpanderToggled)));

            //POI List Dijit
            this.poiList = new POIList({
                map: this.map,
                config: this.config,
                nls: this.nls
            });

            this.poiList.own(on(this.poiList, "expanderToggled", lang.hitch(this, this.onAnyExpanderToggled)));

            this.poiList.placeAt(this.poiListNode);
            this.poiList.startup();
        },

        startup:function(){
            this.inherited(arguments);
            this.doCustomNodeResizing();
        },

        onCentreSelected: function (args) {
            if (args.centrePoint) {
                this.bufferGeometry = args.bufferGeometry;
                this.poiGrid.startSelectFeatures(args.centrePoint, args.bufferGeometry);
            }
            else{//empty args = message to clear features
                this.poiGrid.clearFeatures();
                this.featureSelectLayer.clear();
                this.poiList.removeAllPois();
            }
        },

        onPOIChecked: function (args) {
            this.poiList.changePoiList(args);
        },

        onAnyExpanderToggled: function(args){
            this.doCustomNodeResizing();
        },

        setContainerDimensions: function(containerDimensions){
            this.containerDimensions = containerDimensions;
        },

        resizedContainer: function (containerDimensions) {
            if (this.containerDimensions == null ||
                this.containerDimensions.height != containerDimensions.height ||
                this.containerDimensions.width != containerDimensions.width) {
                //change in dimension detected, compared to previous stored
                this.setContainerDimensions(containerDimensions);

                this.doCustomNodeResizing();
            }
        },

        doCustomNodeResizing: function () {
            if (this.containerDimensions ==  null){
                return;
            }

            var centrepointDetails = this.centrePoint.getNodeCollapseStateAndHeight();
            var poiGridDetails = this.poiGrid.getNodeCollapseStateAndHeight();
            var poiListDetails = this.poiList.getNodeCollapseStateAndHeight();
            var marginBetweenNodes = 5;

            var lowerTwoThirdNodesHeight = this.containerDimensions.height - centrepointDetails.height;

            if (lowerTwoThirdNodesHeight < this.containerDimensions.height / 2) {
                //Container height is too short, allow overflow
                this.poiGrid.setNodeDefaultMaxHeight();
                this.poiList.setNodeDefaultMaxHeight();
            }
            else {
                var gridAndListHeight = (this.containerDimensions.height - centrepointDetails.height
                - marginBetweenNodes * 3 - 15);

                if (!poiGridDetails.collapseState && !poiListDetails.collapseState) {
                    //Divide the remaining space into two between the Grid and List nodes
                    var gridOrListHeight = gridAndListHeight / 2;

                    this.poiGrid.setNodeMaxHeight(gridOrListHeight);
                    this.poiList.setNodeMaxHeight(gridOrListHeight);
                }
                else if (!poiGridDetails.collapseState) {
                    var gridHeight = gridAndListHeight - poiListDetails.height;
                    this.poiGrid.setNodeMaxHeight(gridHeight);
                }
                else if (!poiListDetails.collapseState) {
                    var listHeight = gridAndListHeight - poiGridDetails.height;
                    this.poiList.setNodeMaxHeight(listHeight);
                }
            }
        }
    });
});