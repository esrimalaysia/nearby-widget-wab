define({
    root: ({
        _widgetLabel: "Nearby",
        title: "Map Title",
        findCentrePointTitle:"Specify Location",
        findCentrePoint_Clear:"Clear",
        findCentrePoint_SearchAddress:"Find address or location",
        findCentrePoint_SpecifyLocation:"Specify Location",
        bufferSliderText:"Show results within radius of ",
        bufferSliderUnit:"km",
        nearbyBufferRadiusTextboxTooltip:"Enter radius, then press 'Enter'",
        POIGridTitle:"Points of Interest Categories",
        POIListTitle:"Points of Interest List",
        searchFilterButton:"Search/Filter",
        searchTextBox:"Search:",
        allFieldsSearch:"All fields",
        filterResultsToggleTooltip:"Toggle filtered results",
        poiCountSuffix: " POIs",
        searchTooltipInfoTitle: "Search Info",
        searchTooltipInfoContent: "<b>Note</b><br/>Please use commas(,) to search with multiple tags.<br/>E.g. Enter <i>RHB Bank, Maybank</i> to search for both <i>'RHB Bank'</i> and <i>'Maybank'</i> bank points of interest.",
        searchAdvancedSectionTitle: "Advanced Search",
        selectSearchFields: "Select Fields:",
        otherSearchSettings: "Other Settings:",
        searchCurrentExtent: "Search Features in Map View Only"
    })
});