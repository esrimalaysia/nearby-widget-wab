define([
        'dojo/_base/declare',
        'jimu/BaseWidget',
        'jimu/portalUtils',
        'dojo/_base/lang',
        'dojo/Deferred',
        "dojo/on",
        "jimu/dijit/Message",
        './Nearby'
    ],
    function ( declare,
               BaseWidget,
               portalUtils,
               lang,
               Deferred,
               on,
               Message,
               Nearby) {
        return declare([BaseWidget], {
            baseClass: 'jimu-widget-nearby',
            name: 'Nearby',
            className: 'esri.widgets.Nearby',
            resizeId: null,

            startup: function () {
                // summary:
                //    this function will be called when widget is started.
                // description:
                //    see dojo's dijit life cycle.
                this.inherited(arguments);
                this.nearby = new Nearby({
                    map: this.map,
                    appConfig: this.appConfig,
                    config: this.config,
                    nls: this.nls
                });
                this.nearby.placeAt(this.nearbyNode);
                this.nearby.setContainerDimensions(this.getContainerDimensions());
                this.nearby.startup();

                // When window resizes, redraw the three nearby nodes with height proportionally adjusted
                window.onresize = lang.hitch(this, function () {
                    clearTimeout(this.resizeId);
                    this.resizeId = setTimeout(lang.hitch(this,this.doneResizing), 300);
                });
            },

            doneResizing: function(){
                this.nearby.resizedContainer(this.getContainerDimensions());
            },

            getContainerDimensions: function() {
                var containerNode = this.nearbyWIdget.parentNode.parentNode;
                return {height: containerNode.clientHeight, width:containerNode.clientWidth};
            }
        });
    });