/**
 * Created by mkmoey on 17/5/2016.
 */
define([
    'dojo/_base/declare',
    "require",
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./../templates/POICell.html',
    './PointOfInterest',
    'dojo/on',
    'dojo/_base/lang',
    "dojo/parser",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/dom",
    "dojo/query",
    "dojo/Evented",
    "dijit/form/CheckBox",
    "esri/geometry/geometryEngine",
    "esri/layers/FeatureLayer",
    "esri/tasks/query",
    "esri/InfoTemplate",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/Color"
], function (declare,
             require,
             _WidgetBase,
             _TemplatedMixin,
             _WidgetsInTemplateMixin,
             POICellTemplate,
             PointOfInterest,
             on,
             lang,
             parser,
             domAttr,
             domStyle,
             dom,
             query,
             Evented,
             CheckBox,
             geometryEngine,
             FeatureLayer,
             Query,
             InfoTemplate,
             Graphic,
             SimpleMarkerSymbol,
             SimpleLineSymbol,
             Color) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
        map: null,
        baseClass: "POICellDijit",
        widgetsInTemplate: true,
        templateString: POICellTemplate,
        poiConfig: null,
        poiCategoryName: null,
        isChecked: false,
        bufferGeometry: null,
        centrePoint: null,
        featureSelectGraphicLayer: null,
        featureLayer: null,
        featureHilightColour: null,
        featureHilightLineWidth: 5,
        selectedGraphicsCache: [],
        selectedPOIs: [],
        distanceUnits: "kilometers",
        subTypeDictionary: {},

        postCreate: function () {
            this.inherited(arguments);
            var nodeId = this.poiConfig.name + "_poi_node";

            this.featureHilightColour = new Color(this.poiConfig.highlightColour);
            this.featureHilightColour.a = this.poiConfig.highlightTransparency;

            if (this.poiConfig.name.length > 13) {//change styling to accommodate 2 rows
                dojo.addClass(this.POICellTitle, "poiCellTitleDoubleRow");
            }

            domAttr.set(this.POICellNode, "id", nodeId);
            domAttr.set(this.POICellNode, "title", this.poiConfig.name);
            this.POICellTitle.innerHTML = this.poiConfig.name;
            this.POICellImageNode.src = require.toUrl(this.poiConfig.iconPath);

            this.poiCategoryName = this.poiConfig.name.replace(/ /g, '');

            if (this.poiConfig.infoTemplate) {//infotemplate for a custom feature popup info
                featureInfoTmplt = new InfoTemplate(this.poiConfig.infoTemplate);
            }
            else {
                featureInfoTmplt = new InfoTemplate("${*}");//print out all attributes and values
            }

            if (this.poiConfig.outputFields) {
                outputFields = this.poiConfig.outputFields;
            }
            else {
                outputFields = "*";//all fields (wasteful)
            }

            if (this.poiConfig.layerUrl) {
                this.featureLayer = new FeatureLayer(this.poiConfig.layerUrl, {
                    infoTemplate: featureInfoTmplt,
                    outFields: outputFields,
                });
            }

            if (this.poiConfig.definitionExpression &&
                this.poiConfig.definitionExpression != null &&
                this.poiConfig.definitionExpression != "") {
                this.featureLayer.setDefinitionExpression(this.poiConfig.definitionExpression);
            }
        },

        getFeatureLayerSubtype: function () {
            //Some fields might be a subtype (number without actual words)
            //So create a dictionary type object for reference in the POI row
            //This is a basically a HACK method, as the data is found not by conventional means

            if (this.featureLayer.renderer && this.featureLayer.renderer.attributeField && this.subTypeDictionary !== {}) {
                var symbolDict = {};

                var setSymbolDict = function (key, featureLayer) {
                    symbolDict[key] = featureLayer.renderer._symbols[key].label;
                }

                for (var key in this.featureLayer.renderer._symbols) {
                    if (this.featureLayer.renderer._symbols.hasOwnProperty(key)) {
                        lang.hitch(this, setSymbolDict(key, this.featureLayer));
                    }
                }
                this.subTypeDictionary[this.featureLayer.renderer.attributeField] = symbolDict;
            }
        },

        poiCellClick: function () {
            if (this.POICellCheckbox.checked) {//toggle the checkbox with every click
                //CHECKBOX ==> UNCHECKED
                this.POICellCheckbox.set('checked', false);
                this.isChecked = false;
                dojo.removeClass(this.POICellNode, "POICellDijitSelected");
                this.deleteFeatures();

                this.emit("POICellChecked", {"checked": false, "category": this.poiCategoryName});
            }
            else {
                //CHECKBOX ==> CHECKED
                this.POICellCheckbox.set('checked', true);
                this.isChecked = true;
                dojo.addClass(this.POICellNode, "POICellDijitSelected");

                //add back the cached items since selected buffer is the same
                if (this.selectedGraphicsCache.length > 0) {
                    this.featureSelectGraphicLayer.suspend();

                    this.selectedGraphicsCache.forEach(lang.hitch(this, function (feat) {
                        this.featureSelectGraphicLayer.add(feat);
                    }));

                    this.featureSelectGraphicLayer.resume();
                    this.emitSelectedPOIs();
                }
                else {
                    if (this.bufferGeometry !== null) {
                        this.queryFeatures(this.bufferGeometry);
                    }
                }
            }
        },

        deleteFeatures: function () {
            var indexFeat;
            this.featureSelectGraphicLayer.suspend();
            for (indexFeat = this.featureSelectGraphicLayer.graphics.length - 1; indexFeat >= 0; --indexFeat) {
                if (this.featureSelectGraphicLayer.graphics[indexFeat].attributes.Category == this.poiCategoryName) {
                    this.featureSelectGraphicLayer.remove(this.featureSelectGraphicLayer.graphics[indexFeat]);
                }
            }
            this.featureSelectGraphicLayer.resume();
        },

        selectFeatures: function (centrePoint, bufferGeo) {
            this.PoiCatFeatureCount.innerHTML = null;
            if (this.bufferGeometry == null || !geometryEngine.equals(this.bufferGeometry, bufferGeo)) {
                this.bufferGeometry = bufferGeo;
                this.centrePoint = centrePoint;
                this.clearSelectedCaches();
                this.deleteFeatures();
                if (this.isChecked == true) {
                    this.queryFeatures(this.bufferGeometry);
                }
            }
        },

        clearFeatures: function () {
            this.PoiCatFeatureCount.innerHTML = null;
            this.bufferGeometry = null;
            this.centrePoint = null;
            this.POICellCheckbox.set('checked', false);
            this.isChecked = false;
            dojo.removeClass(this.POICellNode, "POICellDijitSelected");
            this.clearSelectedCaches();
        },

        emitSelectedPOIs: function () {
            this.emit("POICellChecked", {
                "checked": true,
                "category": this.poiCategoryName,
                "fields": this.fields,
                "pois": this.selectedPOIs
            });
        },

        queryFeatures: function (bufferGeometry) {
            var query = new Query();
            query.geometry = bufferGeometry;
            query.returnGeometry = true;
            //use a fast bounding box query. will only go to the server if bounding box is outside of the visible map
            this.featureLayer.queryFeatures(query, lang.hitch(this, this.selectInBuffer));
        },

        clearSelectedCaches: function () {
            this.selectedGraphicsCache = [];
            this.selectedPOIs = [];
        },

        checkSubtypeDictionary: function (columnName, attributes) {
            //check to see if the value retrieved is a number, not a description of subtype column value
            //if it is, return the associated label for that subtype, recorded earlier in subTypeDictionary
            if (this.subTypeDictionary[columnName]) {
                return this.subTypeDictionary[columnName][attributes[columnName]];
            }
            else {
                return attributes[columnName];
            }
        },

        replaceAttributeSubtype: function(attributes){
            for (var attribute in attributes) {
                if (attributes.hasOwnProperty(attribute)) {
                    attributes[attribute] = this.checkSubtypeDictionary(attribute, attributes);
                }
            }
            return attributes;
        },

        selectInBuffer: function (response) {
            var feature;
            var features = response.features;
            this.fields = response.fields;
            this.clearSelectedCaches();

            lang.hitch(this, this.getFeatureLayerSubtype());

            for (var i = 0; i < features.length; i++) {
                feature = features[i];
                switch (feature.geometry.type) {
                    case "point":
                        var featureHighlightSymbol = new SimpleMarkerSymbol()
                            .setStyle(SimpleMarkerSymbol.STYLE_CIRCLE)
                            .setColor(new Color([0, 0, 0, 0]))
                            .setSize(this.poiConfig.highlightSize)
                            .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                                this.featureHilightColour, this.featureHilightLineWidth));
                        var graphicFeature = new Graphic(feature.geometry,
                            featureHighlightSymbol,
                            {"Category": this.poiCategoryName});

                        var poi = new PointOfInterest({
                            map: this.map,
                            nls: this.nls,
                            featureSelectGraphicLayer: this.featureSelectGraphicLayer,
                            poiConfig: this.poiConfig,
                            poiCategoryName: this.poiCategoryName,
                            attributes: this.replaceAttributeSubtype(feature.attributes),
                            geometry: feature.geometry,
                            subTypeDictionary: this.subTypeDictionary,
                            distanceToCentre: geometryEngine.distance(feature.geometry,
                                this.centrePoint, this.distanceUnits).toFixed(2)
                        });
                        this.selectedPOIs.push(poi);

                        this.selectedGraphicsCache.push(graphicFeature);
                        this.featureSelectGraphicLayer.add(graphicFeature);

                        graphicFeature.setInfoTemplate(poi.getInfoTemplate());

                        break;
                }
            }
            this.PoiCatFeatureCount.innerHTML = features.length.toString();
            this.emitSelectedPOIs();
        }

    });
});