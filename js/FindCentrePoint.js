/**
 * Created by mkmoey on 6/5/2016.
 */


define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./../templates/FindCentrePoint.html',
    "esri/dijit/LocateButton",
    "esri/dijit/Search",
    "esri/geometry/Point",
    "esri/geometry/Circle",
    "esri/Color",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/graphic",
    "esri/toolbars/draw",
    "esri/graphicsUtils",
    "esri/geometry/webMercatorUtils",
    'dojo/on',
    'dojo/_base/lang',
    "dojo/_base/connect",
    "dojo/parser",
    "dojo/dom-attr",
    "dojo/dom",
    "dojo/Evented",
    "dijit/form/Button",
    "dijit/form/HorizontalSlider"
], function (declare,
             _WidgetBase,
             _TemplatedMixin,
             _WidgetsInTemplateMixin,
             centrePointTemplate,
             LocateButton,
             Search,
             Point,
             Circle,
             Color,
             SimpleMarkerSymbol,
             SimpleLineSymbol,
             SimpleFillSymbol,
             Graphic,
             Draw,
             graphicsUtils,
             webMercatorUtils,
             on,
             lang,
             Connect,
             parser,
             domAttr,
             dom,
             Evented,
             Button,
             HorizontalSlider) {
    parser.parse();
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
        map: null,
        baseClass: "findCentrePointDijit",
        widgetsInTemplate: true,
        templateString: centrePointTemplate,
        nearbyXnBufferLineColour: new Color([255, 120, 20]),
        nearbyXnBufferLineThickness: 3.5,
        nearbyXsize: 13,
        nearbyBufferFillColour: new Color([80, 80, 255, 0.05]),
        radiusUnit: 'esriKilometers',
        graphicNameXmarkNearby: "XmarksTheSpot",
        graphicNameBufferCircleNearby: "BufferCircle",
        config: null,
        bufferRadius: 0,
        currentPoint: null,

        postCreate: function () {
            this.inherited(arguments);

            this.geoLocateCurrent = new LocateButton({
                map: this.map,
                highlightLocation: false
            }, this.currentLocationButton);
            this.geoLocateCurrent.startup();
            this.geoLocateCurrent.own(on(this.geoLocateCurrent, "locate", lang.hitch(this, this.onCurrentLocate)));

            this.searchNearby = new Search({
                map: this.map,
                allPlaceholder: "doesn't work",//this.nls.findCentrePoint_SearchAddress,
                enableHighlight: false,
                enableInfoWindow: false,
                autoNavigate: false//no auto navigation, this app will do it
            }, this.searchLocationNode);
            this.searchNearby.startup();
            this.searchNearby.own(on(this.searchNearby, "select-result", lang.hitch(this, this.onSearchLocate)));

            this.drawPointTool = new Draw(this.map);
            on(this.drawPointTool, "draw-end", lang.hitch(this, this.onSpecifiedPointClickOnMap));

            //setup buffer radius slider
            this.bufferRadiusSlider = new HorizontalSlider({
                name: "slider",
                minimum: this.config.minBufferNearbyRadius,
                maximum: this.config.maxBufferNearbyRadius,
                value: this.config.defaultBufferNearbyRadius,
                intermediateChanges: true,
                "class": "nearbyBufferRadiusSlider"
            }, this.nearbyBufferRadiusSlider);

            // set slider content
            this.setBufferRadiusText();
            // set maximum and minimum value of horizontal slider
            this.nearbyBufferRadiusMinValue.innerHTML = this.bufferRadiusSlider.minimum.toString() + this.nls.bufferSliderUnit;
            this.nearbyBufferRadiusMaxValue.innerHTML = this.bufferRadiusSlider.maximum.toString() + this.nls.bufferSliderUnit;

            // on change event of slider
            this.bufferRadiusSlider.on("change", lang.hitch(this, this.sliderChange));
            // on mouse up event of slider
            this.bufferRadiusSlider.on("mouseUp", lang.hitch(this, this.bufferRadiusSliderMouseUp));

            dojo.connect(dom.byId("nearbyBufferRadiusTextbox"), "onkeyup", lang.hitch(this, this.bufferRadiusTextboxKeyUp));
        },

        onCurrentLocate: function (parameters) {
            if (parameters.error) {
                console.error(parameters.error);
                // new Message({
                //   message: this.nls.failureFinding
                // });
            } else {
                var pointTempNearby = new Point(parameters.position.coords.longitude, parameters.position.coords.latitude);//html5 geolocation api
                this.currentPoint = webMercatorUtils.geographicToWebMercator(pointTempNearby);
                this.addXtoGraphicLayer(pointTempNearby);
            }
        },

        onSearchLocate: function (selectResult) {
            this.currentPoint = new Point(
                selectResult.result.feature.geometry.x,
                selectResult.result.feature.geometry.y,
                selectResult.result.feature.geometry.spatialReference);
            this.addXtoGraphicLayer(this.currentPoint);
        },

        specifyLocationClick: function () {
            this.drawPointTool.activate(Draw.POINT);
            this.disableWebMapPopup();
        },

        onSpecifiedPointClickOnMap: function (evt) {
            this.drawPointTool.deactivate();
            this.enableWebMapPopup();

            if (evt.geometry.type == "point") {
                this.currentPoint = new Point(
                    evt.geometry.x,
                    evt.geometry.y,
                    evt.geometry.spatialReference);
                this.addXtoGraphicLayer(this.currentPoint);
            }
        },

        addXtoGraphicLayer: function (point) {
            this.xBufferLayer.clear();//clear X and buffer, as new point has been chosen

            var graphicForTheX = new Graphic(point,
                new SimpleMarkerSymbol()
                    .setStyle(SimpleMarkerSymbol.STYLE_X)
                    .setSize(this.nearbyXsize)
                    .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                        this.nearbyXnBufferLineColour, this.nearbyXnBufferLineThickness)),
                {"Name": this.graphicNameXmarkNearby}
            );
            this.drawBufferToGraphicLayer();
            this.xBufferLayer.add(graphicForTheX);
        },

        setBufferRadiusText: function () {
            this.bufferRadius = parseFloat(this.bufferRadiusSlider.get('value')).toFixed(2);
            domAttr.set(this.nearbyBufferRadiusText, "innerHTML",
                this.nls.bufferSliderText + "<b>" + this.bufferRadius + this.nls.bufferSliderUnit) + "</b>";
        },

        sliderChange: function (evt) {
            this.setBufferRadiusText();
        },

        bufferRadiusSliderMouseUp: function (evt) {
            this.clearAndDrawBufferToGraphics();
        },

        bufferRadiusTextboxKeyUp: function (evt) {
            if (evt.keyCode == 13) {//check for enter key
                function isNumeric(n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
                }

                var bufferRadiiusText = domAttr.get(dom.byId("nearbyBufferRadiusTextbox"), "value");
                if (isNumeric(bufferRadiiusText)) {//if is a number
                    if (parseFloat(bufferRadiiusText) <= this.bufferRadiusSlider.maximum && //check if within limits
                        parseFloat(bufferRadiiusText) >= this.bufferRadiusSlider.minimum) {
                        this.bufferRadius = parseFloat(bufferRadiiusText).toFixed(2);
                        this.bufferRadiusSlider.set('value', this.bufferRadius);
                        domAttr.set(this.nearbyBufferRadiusText, "innerHTML",
                            this.nls.bufferSliderText + this.bufferRadius + this.nls.bufferSliderUnit);
                        this.clearAndDrawBufferToGraphics();
                    }
                }
                domAttr.set(dom.byId("nearbyBufferRadiusTextbox"), "value", "");//reset textbox
            }
        },

        clearAndDrawBufferToGraphics: function () {
            this.clearBufferGraphicOnly();
            if (this.currentPoint) {
                this.drawBufferToGraphicLayer();
            }
        },

        clearBufferGraphicOnly: function () {
            var indexXBuf;
            for (indexXBuf = this.xBufferLayer.graphics.length - 1; indexXBuf >= 0; --indexXBuf) {
                if (this.xBufferLayer.graphics[indexXBuf].attributes.Name == this.graphicNameBufferCircleNearby) {
                    this.xBufferLayer.remove(this.xBufferLayer.graphics[indexXBuf]);
                }
            }
        },

        drawBufferToGraphicLayer: function () {
            var circleSymb = new SimpleFillSymbol(
                SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_SOLID,
                    this.nearbyXnBufferLineColour,
                    this.nearbyXnBufferLineThickness),
                this.nearbyBufferFillColour);

            var circleObject = new Circle({
                center: this.currentPoint,
                geodesic: true,
                radius: this.bufferRadius,
                radiusUnit: this.radiusUnit
            });
            var circleGraphic = new Graphic(circleObject, circleSymb, {"Name": this.graphicNameBufferCircleNearby});
            this.xBufferLayer.add(circleGraphic);
            this.setMapExtentToBuffers();
        },

        setMapExtentToBuffers: function () {
            var myBufferExtent = graphicsUtils.graphicsExtent(this.xBufferLayer.graphics);
            this.map.setExtent(myBufferExtent);

            for (indexXBuf = this.xBufferLayer.graphics.length - 1; indexXBuf >= 0; --indexXBuf) {
                if (this.xBufferLayer.graphics[indexXBuf].attributes.Name == this.graphicNameBufferCircleNearby) {
                    bufferGeometry = this.xBufferLayer.graphics[indexXBuf].geometry;
                }
            }

            //trigger event of the centre point being selected
            this.emit("centreSelected", {"bufferGeometry": bufferGeometry, "centrePoint": this.currentPoint});
        },

        enableWebMapPopup: function () {
            if (this.map) {
                this.map.setInfoWindowOnClick(true);
            }
        },

        disableWebMapPopup: function () {
            if (this.map) {
                this.map.setInfoWindowOnClick(false);
            }
        },

        getNodeCollapseStateAndHeight: function () {
            if (!this.centrePointToggle.checked) {
                //when expanded, make overflow visible, so that search suggestions menu fully seen
                this.horizontalMenuNode.style = "overflow: visible";
            }
            else{
                this.horizontalMenuNode.style = "";
            }

            return {collapseState: this.centrePointToggle.checked, height: this.findCentrePointDijit.offsetHeight};
        },

        onExpanderToggled: function () {
            this.emit("expanderToggled", {collapseState: this.centrePointToggle.checked, node: "FindCentrePoint"});
        },

        clearClick: function () {
            this.drawPointTool.deactivate();
            this.xBufferLayer.clear();
            this.currentPoint = null;
            this.emit("centreSelected", {"centrePoint": null});
        }

    });
});