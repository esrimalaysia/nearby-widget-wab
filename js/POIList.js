/**
 * Created by mkmoey on 10/6/2016.
 */
define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./../templates/POIList.html',
    './PointOfInterest',
    'dojo/on',
    'dojo/_base/lang',
    "dojo/_base/connect",
    "dojo/parser",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/dom",
    "dojo/query",
    "dojo/Evented",
    "dojo/dom-construct",
    "dojo/keys",
    "dijit/registry",
    "dijit/TooltipDialog",
    "dijit/popup",
    "dijit/form/DropDownButton",
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/form/CheckBox",
    "dijit/TitlePane",
    "dijit/Dialog",
    "dojo/store/Memory",
    'dgrid/OnDemandList'
], function (declare,
             _WidgetBase,
             _TemplatedMixin,
             _WidgetsInTemplateMixin,
             POIListTemplate,
             PointOfInterest,
             on,
             lang,
             Connect,
             parser,
             domAttr,
             domStyle,
             dom,
             query,
             Evented,
             domConstruct,
             keys,
             registry,
             TooltipDialog,
             popup,
             DropDownButton,
             TextBox,
             Button,
             CheckBox,
             TitlePane,
             Dialog,
             Memory,
             OnDemandList) {
    parser.parse();
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
        map: null,
        baseClass: "POIListDijit",
        widgetsInTemplate: true,
        templateString: POIListTemplate,
        config: null,
        poiStore: null,
        poiList: null,
        firstTimePopulate: true,
        fieldsListByCat: [],
        fieldCheckBoxDijitList: [],
        timeoutId: null,
        useFilter: false,
        useMapExtentFilter: true,
        currentSearchParameters: null,
        currentMapExtent: null,
        storedCheckboxState: {all: true, checkedList: []},

        postCreate: function () {
            this.inherited(arguments);
            this.poiStore = new Memory();
            this.nearbyPoiListContainer.parentNode.style = "padding: 0;";//remove padding of this node's parent by javascript

            on(this.nearbySearchTooltipDialog, "open", lang.hitch(this, this.onSearchDialogOpen));

            String.prototype.format = function () {
                var formatted = this;
                for (var i = 0; i < arguments.length; i++) {
                    var regexp = new RegExp('\\{' + i + '\\}', 'gi');
                    formatted = formatted.replace(regexp, arguments[i]);
                }
                return formatted;
            };

            this.map.on("extent-change",lang.hitch(this, this.onMapExtentChange));
        },

        onMapExtentChange: function(event){
            this.currentMapExtent = event.extent;
            this.refreshPoiList();
        },

        changePoiList: function (args) {
            if (args["checked"]) {
                this.removePois(args["category"]);
                var indexPois;
                for (indexPois = args.pois.length - 1; indexPois >= 0; --indexPois) {
                    this.poiStore.put(args.pois[indexPois], {id: args.pois[indexPois].id});
                }

                this.addFieldsList(args["category"], args["fields"]);
            }
            else {//unchecked: remove all poi by category
                this.removePois(args["category"]);
                this.removeFieldsList(args["category"]);
            }

            this.initializeList();
            this.poiList.set("sort", this.sortByDistance);
            this.refreshPoiList();

            if (this.firstTimePopulate) {//first time got issue, not getting size setup right
                this.firstTimePopulate = false;
                this.emit("expanderToggled", {collapseState: this.nearbyListToggle.checked, node: "POIList"})
            }
        },

        removePois: function (categoryName) {
            this.poiStore.query({poiCategoryName: categoryName}).forEach(lang.hitch(this, function (poi) {
                this.poiStore.remove(poi.id);
            }));
        },

        removeAllPois: function () {
            this.poiStore.query({}).forEach(lang.hitch(this, function (poi) {
                this.poiStore.remove(poi.id);
            }));
            this.refreshPoiList();
            this.fieldsListByCat.splice(0, this.fieldsListByCat.length);
        },

        addFieldsList: function (category, fields) {
            if (this.fieldsListByCat.length > 1) {//removing possible preexisting field object
                this.removeListItem(category, "category", this.fieldsListByCat);
            }

            //add new field object
            this.removeListItem("objectid", "name", fields);//remove all objectid fields always
            this.fieldsListByCat.push({"category": category, "fields": fields});
        },

        removeFieldsList: function (category) {
            this.removeListItem(category, "category", this.fieldsListByCat);
        },

        removeListItem: function (valueToMatch, propertyToMatch, listForRemoval) {
            if (!listForRemoval)
                return;

            //remove all items in list that match the attribute value
            var itemsToRemove = [];
            for (var i = listForRemoval.length - 1; i >= 0; i--) {
                if (listForRemoval[i][propertyToMatch] == valueToMatch) {
                    itemsToRemove.push(i);
                }
            }
            itemsToRemove.sort(function (a, b) {
                return b - a//sort descending
            });
            for (index in itemsToRemove) {
                listForRemoval.splice(itemsToRemove[index], 1);
            }
            var test = 1;
        },

        initializeList: function () {
            if (this.poiList === null) {
                this.poiList = new OnDemandList({
                    store: this.poiStore,
                    query: lang.hitch(this, this.filterQuery),
                    renderRow: function (object, options) {
                        object.startup();
                        return object.domNode;
                    }
                }, this.nearbyPoiList);
            }
        },

        sortByDistance: function (a, b) {//sort poi by ascending distance
            if (a.distanceToCentre > b.distanceToCentre) {
                return 1;
            }
            if (a.distanceToCentre < b.distanceToCentre) {
                return -1;
            }
            return 0;
        },

        onExpanderToggled: function () {
            this.emit("expanderToggled", {collapseState: this.nearbyListToggle.checked, node: "POIList"});
        },

        getNodeCollapseStateAndHeight: function () {
            return {collapseState: this.nearbyListToggle.checked, height: this.POIListDijit.offsetHeight};
        },

        setNodeMaxHeight: function (height) {
            this.setMaxHeight(height);
        },

        setNodeDefaultMaxHeight: function () {
            defaultMaxHeight = 1000;
            this.setMaxHeight(defaultMaxHeight);
        },

        setMaxHeight: function (height) {
            var dscroller = query(".jimu-widget-nearby .POIListDijit .dgrid-scroller");
            var dlist = query(".jimu-widget-nearby .POIListDijit .dgrid-list");

            var contentHeight = height - this.subHeadingTitle.clientHeight -
                this.nearbySearchBar.clientHeight - 2;//2 is margin bottom in css

            if (dscroller[0]) {
                domStyle.set(dscroller[0], {"max-height": contentHeight + "px"});
                domStyle.set(dlist[0], {"max-height": contentHeight + "px"});
                domStyle.set(dlist[0], {"height": contentHeight + "px"});//need to set normal height to override dgrid 30em height
            }
        },

        onSearchDialogOpen: function () {
            //get list of fields from all active categories
            var getUniqueFieldList = function (fieldsListByCat) {
                var doesListContain = function (field, fieldList) {
                    for (var i = 0; i < fieldList.length; i++) {
                        isFieldItemExist = false;
                        for (var item in fieldList) {
                            if (fieldList[item]["name"] == field["name"] && fieldList[item]["alias"] == field["alias"]) {
                                isFieldItemExist |= true;
                            }
                            else {
                                isFieldItemExist |= false;
                            }
                        }
                        if (isFieldItemExist)
                            return true;
                    }
                    return false;
                };

                var uniqueFieldList1 = [];
                for (categorryIdx in fieldsListByCat) {
                    var category = fieldsListByCat[categorryIdx];
                    for (field in category.fields) {
                        var newFieldEntry = {
                            "name": category.fields[field].name,
                            "alias": category.fields[field].alias
                        };
                        if (!doesListContain(newFieldEntry, uniqueFieldList1)) {
                            uniqueFieldList1.push(newFieldEntry);
                        }
                    }
                }
                return uniqueFieldList1;
            };

            uniqueFieldList = getUniqueFieldList(this.fieldsListByCat);

            //clear existing checkbox list
            dojo.forEach(dijit.findWidgets(dojo.byId('nearbySearchCheckboxList')), function (w) {
                w.destroyRecursive();
            });
            domConstruct.empty("nearbySearchCheckboxList");//clear node
            this.fieldCheckBoxDijitList = [];

            //populate checkbox list
            for (field in uniqueFieldList) {
                var checkboxDiv = "cb_nb_" + field;
                var checkboxDijitId = checkboxDiv + "dijit";

                domCheckBoxRow = '<div id="{0}"></div><label for="{1}">{2}</label><br/>'.format(checkboxDiv, checkboxDijitId, uniqueFieldList[field].alias);

                var row = domConstruct.toDom(domCheckBoxRow);
                domConstruct.place(row, this.nearbySearchCheckboxList);

                targetCheckbox = query("#" + checkboxDiv);

                //check whether to precheck the checkbox based on last state of checkboxes
                cbState = this.storedCheckboxState.checkedList.includes(uniqueFieldList[field].name) ? true : false;

                var checkBox = new CheckBox({
                    id: checkboxDijitId,
                    value: uniqueFieldList[field].name,
                    checked: cbState,
                }, targetCheckbox[0]).startup();

                this.fieldCheckBoxDijitList.push(registry.byId(checkboxDijitId));
            }

            //set allcheckbox check state
            checkedMarkup = this.storedCheckboxState.all ? 'checked' : false;
            domAttr.set(registry.byId("nearbySearchAllCheckbox"), "checked", checkedMarkup);
            this.onAllCheckChange(this.storedCheckboxState.all);
        },

        onAllCheckChange: function (param) {//if 'all fields' are checked, disable all fields below, and vice versa
            for (dijiti in this.fieldCheckBoxDijitList) {
                disabledMarkup = param ? 'disabled' : false;
                this.fieldCheckBoxDijitList[dijiti].set('disabled', disabledMarkup);
            }
        },

        onSearchTooltipClose: function () {
            var allChecked = domAttr.get(dom.byId("nearbySearchAllCheckbox"), "checked");
            this.storedCheckboxState.all = allChecked;

            var checkedFields = [];
            for (dijiti in this.fieldCheckBoxDijitList) {
                if (this.fieldCheckBoxDijitList[dijiti].checked) {
                    checkedFields.push(this.fieldCheckBoxDijitList[dijiti].value);
                }
            }
            this.storedCheckboxState.checkedList = checkedFields;
        },

        onSearchInfoClick: function () {
            var searchInfoDialog = new Dialog({
                title: this.nls.searchTooltipInfoTitle,
                style: "width: 300px;",
                content: this.nls.searchTooltipInfoContent
            });
            searchInfoDialog.show();
        },

        onSearchClick: function () {
            this.executeSearch();
        },

        onSearchTextBoxKeyDown: function (event) {
            //query returns a nodelist, which has an on() function available that will listen
            //to all keydown events for each node in the list
            switch (event.keyCode) {
                case keys.ENTER:
                    this.executeSearch();
                    break;
            }
        },

        executeSearch: function () {
            var filterString = domAttr.get(dom.byId("nearbySearchTextBox"), "value");
            var filterFields = this.getFilterFields();

            if (!filterString || filterString == "" || filterString == null ||
                filterString.length == 0 || filterFields.length == 0) {
                this.currentSearchParameters = null;
                domAttr.remove(dojo.byId("nearbyFilterToggle"), "checked");

                this.useFilter = false;
            }
            else {
                var filterStringObject = this.filterStringObject(filterString);
                this.currentSearchParameters = {filterFields: filterFields, filterStringObject: filterStringObject};
                domAttr.set(dojo.byId("nearbyFilterToggle"), "checked", true);
                this.useFilter = true;
            }

            popup.close(this.nearbySearchTooltipDialog);

            this.refreshPoiList();
        },

        filterStringObject: function (filterString) {
            var filterStringObject = [];

            function getSubStr(str, delim) {
                var a = str.indexOf(delim);
                if (a == -1)
                    return '';

                var b = str.indexOf(delim, a + 1);

                if (b == -1)
                    return '';

                return str.substr(a + 1, b - a - 1);
                //                 ^    ^- length = gap between delimiters
                //                 |- start = just after the first delimiter
            }

            //Step 1: Split by commas
            var commaDelimitedStrings = filterString.trim().split(",");
            for (var i = 0; i < commaDelimitedStrings.length; i++) {
                var commaDelimitedStringList = [];
                //Step 2: Look for pairs of quotation marks
                var quotString = getSubStr(commaDelimitedStrings[i], "\"");
                while (quotString != '') {
                    commaDelimitedStringList.push(quotString);//push found string within quotation (may be multiple)
                    commaDelimitedStrings[i] = commaDelimitedStrings[i].replace("\"" + quotString + "\"", " ");

                    quotString = getSubStr(commaDelimitedStrings[i]);
                }
                //Step 3: Add other strings (within comma delimited phrase) as individual search keyword
                spaceDelimitedStrings = commaDelimitedStrings[i].trim().split(" ");
                for (var j = 0; j < spaceDelimitedStrings.length; j++) {
                    if (spaceDelimitedStrings[j].trim() != '')
                        commaDelimitedStringList.push(spaceDelimitedStrings[j].trim());
                }

                filterStringObject.push(commaDelimitedStringList);
            }

            //this object has 2 tiers:
            //first tier comma delimited phrase object
            //second tier, lists of 'filter strings' to use
            return filterStringObject;
        },

        getFilterFields: function () {
            var allChecked = domAttr.get(dom.byId("nearbySearchAllCheckbox"), "checked");
            var returnFields = [];
            for (dijiti in this.fieldCheckBoxDijitList) {
                if (allChecked || this.fieldCheckBoxDijitList[dijiti].checked) {
                    returnFields.push(this.fieldCheckBoxDijitList[dijiti].value);
                }
            }
            return returnFields;
        },

        filterQuery: function (item, index, items) {
            if (!this.useFilter)
                return true;

            if(this.useMapExtentFilter){
                if (!this.currentMapExtent.contains(item.geometry))
                    return false;
            }

            if (this.currentSearchParameters == null)
                return true;

            //function to search each field of this POI, return true if any found (OR functionality)
            var searchEachField = function (filterString, item, filterFields) {
                for (var j = 0; j < filterFields.length; j++) {
                    {
                        field = filterFields[j];
                        if (item.attributes[field] &&
                            item.attributes[field].toLowerCase().includes(filterString.toLowerCase())) {
                            return true;
                        }
                    }
                }
                return false;
            };

            //search each filter string, return true if all found (AND functionality)
            var searchEachFilter = function(item, filterStringList, filterFields){
                var matchesCount = 0;
                for (var i = 0; i < filterStringList.length; i++) {
                    filterString = filterStringList[i];
                    if (searchEachField(filterString, item, filterFields))
                        matchesCount++;
                }
                return matchesCount == filterStringList.length? true: false;
            };

            //search each comma delimited phrase, return true (OR functionality)
            for (var h = 0; h < this.currentSearchParameters.filterStringObject.length; h++) {
                if (searchEachFilter(item,
                        this.currentSearchParameters.filterStringObject[h],
                        this.currentSearchParameters.filterFields))
                    return true;
            }

            return false;
        },

        onFilterToggleClick: function (event) {
            var toggleChecked = domAttr.get(dojo.byId("nearbyFilterToggle"), "checked");
            this.useFilter = toggleChecked;

            this.refreshPoiList();
        },

        refreshPoiList: function () {
            if (this.poiList) {
                this.poiList.refresh();
            }
            else {
                this.totalListPois.innerHTML = "";
                return;
            }

            if (this.poiList._total <= 0)
                this.totalListPois.innerHTML = "";
            else
                this.totalListPois.innerHTML = this.poiList._total + this.nls.poiCountSuffix;
        },

        onSearchCurrentExtentCbChange: function(event){
            this.useMapExtentFilter = event;
        }

    });
});