/**
 * Created by mkmoey on 10/6/2016.
 */
define([
    'dojo/_base/declare',
    "require",
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./../templates/POIRow.html',
    'dojo/on',
    'dojo/_base/lang',
    "dojo/parser",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/dom",
    "dojo/Evented",
    "esri/geometry/geometryEngine",
    "dijit/TooltipDialog",
    "dijit/popup",
    "esri/InfoTemplate",
    "dijit/Menu",
    "dijit/MenuItem",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/Color"
], function (declare,
             require,
             _WidgetBase,
             _TemplatedMixin,
             _WidgetsInTemplateMixin,
             POIRowTemplate,
             on,
             lang,
             parser,
             domAttr,
             domStyle,
             dom,
             Evented,
             geometryEngine,
             TooltipDialog,
             popup,
             InfoTemplate,
             Menu,
             MenuItem,
             Graphic,
             SimpleMarkerSymbol,
             SimpleLineSymbol,
             Color) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
        map: null,
        baseClass: "POIRowDijit",
        widgetsInTemplate: true,
        templateString: POIRowTemplate,
        poiConfig: null,
        poiCategoryName: null,
        featureSelectGraphicLayer: null,
        distanceToCentre: null,
        attributes: null,
        id: null,
        flashFeatName: "Flasher",
        flashFeatColor: new Color([255, 0, 0]),
        flashFeatLineWidth: 5,
        flashFeatSize: 25,
        zoomLevel: null,
        geometry: null,
        subTypeDictionary: null,
        detailsCols: null,
        feature: null,

        postCreate: function () {
            this.inherited(arguments);
            this.id = this.guid();
            this.zoomLevel = this.poiConfig.zoomLevel;
            dojo.addClass(this.POIRowNode, this.poiCategoryName.replace(/ /g, '') + "PoiCategory");

            this.POIRowIconNode.src = require.toUrl(this.poiConfig.iconPath);
            this.POIRowDistanceNode.innerHTML = this.distanceToCentre + "km";

            //get the list of columns
            this.detailsCols = this.poiConfig.detailsColumns.split(',');
            var indexCols;
            for (indexCols = 1; indexCols < this.detailsCols.length; indexCols += 1) {
                this.detailsCols[indexCols] = this.detailsCols[indexCols].trim();//trim each field name
            }

            this.initializeRowDetails(this.detailsCols);
            this.initializeTooltipMatters(this.detailsCols);
            this.initializeContextMenu();
        },

        startup: function () {
            this.inherited(arguments);
        },

        getInfoTemplate: function () {
            var infoTemplate = new InfoTemplate();
            infoTemplate.setTitle(this.attributes[this.detailsCols[0]]);

            infoTemplate.setContent(this.buildInfoWindowContentString());
            return infoTemplate;
        },

        initializeRowDetails: function (detailsCols) {
            var detailsString = "<b>" + this.attributes[detailsCols[0]] + "</b>"
            var indexCols;

            for (indexCols = 1; indexCols < detailsCols.length; indexCols += 1) {
                var colVal = this.attributes[detailsCols[indexCols]];
                if (colVal && colVal !== "" && colVal !== " ") {
                    detailsString = detailsString + " &#8226; " + colVal;
                }
            }
            this.POIRowDetailsNode.innerHTML = detailsString;//set content of list item
        },

        initializeTooltipMatters: function (detailsCols) {

            this.own(on(this.POIRowContainerNode, "mouseout", lang.hitch(this, function () {
                popup.close(this.poiTooltipDialog);
                this.poiTooltipDialog.destroyRecursive();

                var indexFeat;
                for (indexFeat = this.featureSelectGraphicLayer.graphics.length - 1; indexFeat >= 0; --indexFeat) {
                    if (this.featureSelectGraphicLayer.graphics[indexFeat].attributes.Category == this.flashFeatName) {
                        this.featureSelectGraphicLayer.remove(this.featureSelectGraphicLayer.graphics[indexFeat]);
                    }
                }
            })));

            this.own(on(this.POIRowContainerNode, 'mouseover', lang.hitch(this, function () {
                //Tooltip building
                var tooltipString = "<b>" + this.attributes[detailsCols[0]] + "</b><br>" +
                    "<small>" + this.poiConfig.name + "</small><hr>";//add title and subtitle
                tooltipString = this.buildDetailsString(tooltipString, detailsCols);

                this.poiTooltipDialog = new TooltipDialog({
                    class: "poiRowTooltipDialog",
                    content: tooltipString
                });

                popup.open({
                    popup: this.poiTooltipDialog,
                    around: this.POIRowContainerNode
                });

                var featureHighlightSymbol = new SimpleMarkerSymbol()
                    .setStyle(SimpleMarkerSymbol.STYLE_CIRCLE)
                    .setColor(new Color([0, 0, 0, 0]))
                    .setSize(this.flashFeatSize)
                    .setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                        this.flashFeatColor, this.flashFeatLineWidth));
                var graphicFeature = new Graphic(this.geometry,
                    featureHighlightSymbol,
                    {"Category": this.flashFeatName});
                this.featureSelectGraphicLayer.add(graphicFeature);
            })));

            this.own(on(this.POIRowContainerNode, 'dblclick', lang.hitch(this, function () {
                this.map.infoWindow.setTitle(this.attributes[this.detailsCols[0]]);

                this.map.infoWindow.setContent(this.buildInfoWindowContentString());
                this.map.centerAndZoom(this.geometry, this.zoomLevel).then(lang.hitch(this, function () {
                    this.map.infoWindow.show(this.geometry);
                }));
            })));
        },

        initializeContextMenu: function () {
            var pMenu;
            pMenu = new Menu({
                targetNodeIds: [this.POIRowContainerNode]
            });
            pMenu.addChild(new MenuItem({
                label: "Zoom to",
                onClick: lang.hitch(this, function () {
                    this.map.centerAndZoom(this.geometry, this.zoomLevel);
                })
            }));
        },

        guid: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        },

        buildDetailsString: function (builtString, detailsCols) {
            var index;
            for (index = 1; index < detailsCols.length; index += 1) {
                var colName = detailsCols[index].trim();
                builtString += this.attributes[colName] + "<br>";
            }

            return builtString;
        },

        buildInfoWindowContentString: function(){
            var infoWindowContent = "Category: " + this.poiConfig.name + "<br>";
            infoWindowContent = this.buildDetailsString(infoWindowContent, this.detailsCols);
            return infoWindowContent;
        }
    });
});