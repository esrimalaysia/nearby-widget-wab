/**
 * Created by mkmoey on 17/5/2016.
 */
define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./../templates/POIGrid.html',
    './POICategory',
    'dojo/on',
    'dojo/_base/lang',
    "dojo/_base/connect",
    "dojo/parser",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/dom",
    "dojo/Evented"
], function (declare,
             _WidgetBase,
             _TemplatedMixin,
             _WidgetsInTemplateMixin,
             POIGridTemplate,
             POICategory,
             on,
             lang,
             Connect,
             parser,
             domAttr,
             domStyle,
             dom,
             Evented) {
    parser.parse();
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
        map: null,
        baseClass: "POIGridDijit",
        widgetsInTemplate: true,
        templateString: POIGridTemplate,
        config: null,
        poiCategories: [],
        featureSelectGraphicLayer: null,

        postCreate: function () {
            this.inherited(arguments);
            
            var indexPois;

            for (indexPois = 0; indexPois < this.config.poiCategories.length; ++indexPois) {

                var poiConfig = this.config.poiCategories[indexPois];
                poiConfig.poiDefaultZoomLevel = this.config.poiDefaultZoomLevel;//adding zoom level to poi config

                if (this.config.poiCategories[indexPois].hilightSize &&
                    this.config.poiCategories[indexPois].hilightSize != null &&
                    this.config.poiCategories[indexPois].hilightSize != ""){
                    poiConfig.highlightSize = this.config.poiCategories[indexPois].hilightSize;}
                else{
                    poiConfig.highlightSize = this.config.poiHighlightSize;}

                if (this.config.poiCategories[indexPois].hilightColour &&
                    this.config.poiCategories[indexPois].hilightColour != null &&
                    this.config.poiCategories[indexPois].hilightColour != ""){
                    poiConfig.highlightColour = this.config.poiCategories[indexPois].hilightColour;}
                else{
                    poiConfig.highlightColour = this.config.poiHighlightColour;}

                if (this.config.poiCategories[indexPois].zoomLevel &&
                    this.config.poiCategories[indexPois].zoomLevel != null &&
                    this.config.poiCategories[indexPois].zoomLevel != ""){
                    poiConfig.zoomLevel = this.config.poiCategories[indexPois].zoomLevel;}
                else{
                    poiConfig.zoomLevel = this.config.poiDefaultZoomLevel;}

                poiConfig.highlightTransparency = this.config.poiHighlightTransparency;

                this.poiCategories[indexPois]=  new POICategory({
                    map: this.map,
                    nls: this.nls,
                    poiConfig:poiConfig,
                    featureSelectGraphicLayer: this.featureSelectGraphicLayer
                });
                this.poiCategories[indexPois].placeAt(this.nearbyPoiGrid);
                this.poiCategories[indexPois].startup();
                this.poiCategories[indexPois].own(on(this.poiCategories[indexPois],
                    "POICellChecked", lang.hitch(this, this.onPOIChecked)));
            }
            
        },

        startSelectFeatures: function(centrePoint, bufferGeometry){
            for (indexPois = 0; indexPois < this.config.poiCategories.length; ++indexPois) {
                this.poiCategories[indexPois].selectFeatures(centrePoint, bufferGeometry);
            }
        },

        clearFeatures:function(){
            for (indexPois = 0; indexPois < this.config.poiCategories.length; ++indexPois) {
                this.poiCategories[indexPois].clearFeatures();
            }
        },

        onPOIChecked: function(args){
            this.emit("POICellChecked", args);
        },

        onExpanderToggled: function(){
            this.emit("expanderToggled",{collapseState: this.nearbyGridToggle.checked, node:"POIGrid"});
        },

        getNodeCollapseStateAndHeight: function(){
            return {collapseState: this.nearbyGridToggle.checked, height: this.POIGridDijitNode.offsetHeight};
        },

        setNodeMaxHeight: function(height){
            this.setMaxHeight(height);
        },

        setNodeDefaultMaxHeight: function(){
            defaultMaxHeight = 400;
            this.setMaxHeight(defaultMaxHeight);
        },

        setMaxHeight:function(height) {
            var contentHeight = height - this.subHeadingTitle.clientHeight - 2;//2 is margin bottom in css
            domStyle.set(this.nearbyPoiGrid, {"max-height": contentHeight+"px"});
        }

    });
});